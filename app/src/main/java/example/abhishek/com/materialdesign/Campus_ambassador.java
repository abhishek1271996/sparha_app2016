package example.abhishek.com.materialdesign;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

/**
 * Created by Abhishek on 06-Jun-16.
 */
public class Campus_ambassador extends AppCompatActivity implements View.OnClickListener {

    Toolbar mToolbar1, mToolbar2;
    private EditText full_name,college,city,email_id,contact;
    private TextInputLayout inputLayoutfull_name,inputLayoutcollege,inputLayoutcity,inputLayoutemailid,inputLayoutcontact;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.campus_ambassador);

        mToolbar1 = (Toolbar) findViewById(R.id.nested_toolbar_1);
        mToolbar2 = (Toolbar) findViewById(R.id.nested_toolbar_2);

        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.nested_parentframe);


        setSupportActionBar(mToolbar2);
        getSupportActionBar().setTitle("Campus Ambassador");

        mToolbar1.setNavigationIcon(R.drawable.ic_action_menu);
        mToolbar1.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * TODO: Handle Navigation Drawer menu click here
                 */

            }
        });

        mToolbar2.setTitleTextColor(getResources().getColor(android.R.color.tertiary_text_light));

        initialize();

    }

    public void initialize(){
        inputLayoutfull_name = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutcollege = (TextInputLayout) findViewById(R.id.input_layout_city);
        inputLayoutcity = (TextInputLayout) findViewById(R.id.input_layout_college);
        inputLayoutemailid = (TextInputLayout) findViewById(R.id.input_layout_emailid);
        inputLayoutcontact = (TextInputLayout) findViewById(R.id.input_layout_contact_number);
        full_name = (EditText) findViewById(R.id.input_name);
        college = (EditText) findViewById(R.id.input_college);
        city = (EditText) findViewById(R.id.input_city);
        email_id = (EditText) findViewById(R.id.input_email);
        contact = (EditText) findViewById(R.id.input_contact);
        register = (Button) findViewById(R.id.btn_ambassador_regiter);
        register.setOnClickListener(this);
        full_name.addTextChangedListener(new MyTextWatcher(full_name));
        college.addTextChangedListener(new MyTextWatcher(college));
        city.addTextChangedListener(new MyTextWatcher(city));
        email_id.addTextChangedListener(new MyTextWatcher(email_id));
    }

    @Override
    public void onClick(View v) {
        submitform();
    }

    public void submitform(){
        if (!validateName(full_name,inputLayoutfull_name)) {
            return;
        }
        if (!validateName(college,inputLayoutcollege)) {
            return;
        }
        if (!validateName(city,inputLayoutcity)) {
            return;
        }

        if (!validateEmail()) {
            return;
        }
        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateName(EditText editText,TextInputLayout textInputLayout) {
        if (editText.getText().toString().trim().isEmpty()) {
            textInputLayout.setError(getString(R.string.err_msg_name));
            requestFocus(editText);
            return false;
        } else {
            textInputLayout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = email_id.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutemailid.setError(getString(R.string.err_msg_email));
            requestFocus(email_id);
            return false;
        } else {
            inputLayoutemailid.setErrorEnabled(false);
        }

        return true;
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName(full_name,inputLayoutfull_name);
                    break;
                case R.id.input_city:
                    validateName(city,inputLayoutcity);
                    break;
                case R.id.input_college:
                    validateName(college,inputLayoutcollege);
                    break;
                case R.id.input_email:
                    validateEmail();
                    break;
            }
        }
    }
}
