package example.abhishek.com.materialdesign.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import example.abhishek.com.materialdesign.R;

/**
 * Created by Abhishek on 24-May-16.
 */
public class SimpleRecyclerAdapterNothing extends RecyclerView.Adapter<SimpleRecyclerAdapterNothing.VersionViewHolder> {
    List<String> versionModels;
    Boolean isHomeList = false;

    //public static List<String> homeActivitiesList = new ArrayList<String>();
    //public static List<String> homeActivitiesSubList = new ArrayList<String>();
    String[] celebLinks ={};
    String[] newscontent = {"Spardha is the annual Sports festival of IIT (BHU), Varanasi. In its glorious history of over 30 years, Spardha has grown to become the largest and one of the most awaited sports festival of northern India where athletic competition is drawn from throughout the country.","Each year, over a thousand participants compete in an array of sports like hockey, basketball, cricket, boxing, tennis and many more, creating a stunning spectacle of exceptional fervidness in athletic talent.","The event, embraced by one of the best gatherings of celebrated sportspersons and spirited audience, has always enthused a zest for continuously scaling new zeniths in the pursuit of excellence and vibrancy among one and all.","At present, Spardha has a team of over 500 and is also associated with numerous renowned firms and sports organizations. These exhilarating days have many resounding experiences for participants and supporters alike creating a lifetime of memories. Gear up yourself to witness the thrilling and frolicsome SPARDHA.","At present, Spardha has a team of over 500 and is also associated with numerous renowned firms and sports organizations. These exhilarating days have many resounding experiences for participants and supporters alike creating a lifetime of memories. Gear up yourself to witness the thrilling and frolicsome SPARDHA."};
    String[] newsheadline = {"Swacch Bharat Mission","it's patriotism not politics","PM reviews progress of “Swachh Bharat Mission”","Karnataka CM Siddaramaiah criticises Swachh Bharat cess","Bill Gates Says Swachh Bharat Partnership With India One of the Best"};
    //String[] newscontent = {"Cricketer"};
    Context context;
    //OnItemClickListener clickListener;


    public void setHomeActivitiesList(Context context) {
        this.context=context;
        //String[] listArray = context.getResources().getStringArray(R.array.home_activities);
        //String[] subTitleArray = context.getResources().getStringArray(R.array.home_activities_subtitle);
       /* for (int i = 0; i < listArray.length; ++i) {
            homeActivitiesList.add(listArray[i]);
            homeActivitiesSubList.add(subTitleArray[i]);
        }*/
    }

    public SimpleRecyclerAdapterNothing(Context context) {
        isHomeList = true;
        this.context = context;
        setHomeActivitiesList(context);
    }


    public SimpleRecyclerAdapterNothing(List<String> versionModels) {
        isHomeList = false;
        this.versionModels = versionModels;

    }

    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerlist_item, viewGroup, false);
        VersionViewHolder viewHolder = new VersionViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VersionViewHolder versionViewHolder, int i) {
       /* if (isHomeList) {*/
        versionViewHolder.img.setImageResource(R.drawable.ic_no_pic);
        //Picasso.with(context).load(celebLinks[i]).fit().into(versionViewHolder.img);
        versionViewHolder.title.setText(newsheadline[i]);
        versionViewHolder.subTitle.setText(newscontent[i]);
        /*} else {
            versionViewHolder.title.setText(versionModels.get(i));
        }*/
    }

    @Override
    public int getItemCount() {
       /* if (isHomeList)
            return newsheadline == null ? 0 : homeActivitiesList.size();
        else
            return versionModels == null ? 0 : versionModels.size();
    */
        return newsheadline.length;
    }


    class VersionViewHolder extends RecyclerView.ViewHolder {
        CardView cardItemLayout;
        ImageView img;
        TextView title;
        TextView subTitle;

        public VersionViewHolder(View itemView) {
            super(itemView);

            cardItemLayout = (CardView) itemView.findViewById(R.id.card_view_hof1);
            img = (ImageView) itemView.findViewById(R.id.news_img);
            title = (TextView) itemView.findViewById(R.id.tvNewsHeadline);
            subTitle = (TextView) itemView.findViewById(R.id.tvNewsContent);

           /* if (isHomeList) {*/
           // itemView.setOnClickListener(this);
            /*} else {
                subTitle.setVisibility(View.GONE);
            }*/

        }

        /*@Override
        public void onClick(View v) {
            clickListener.onItemClick(v, getPosition());
        }*/
    }

   /* public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }*/

}



